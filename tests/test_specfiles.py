#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function, division, absolute_import, unicode_literals
import os
import shutil

"""
test_rpmbrew-tools
----------------------------------

Tests for `rpmbrew` module.
"""

import unittest
from pathlib import Path
import sys
import shutil


from rpmbrew.specfiles import listspecdir
from rpmbrew.specfiles import find_specs_dir_in_children
from rpmbrew.specfiles import find_specs_dir_in_siblings
from rpmbrew.specfiles import find_spec_file
from rpmbrew.specfiles import find_top_dir

from rpmbrew.exceptions import SpecFileNotFound

def make_mock_rpmbuild_dir(top_dir):
    """Creates the rpm directory"""

    if not isinstance(top_dir, Path):
        top_dir = Path(top_dir)

    #Ensure the top_dir is empty.

    if top_dir.as_posix().startswith("/var/tmp"):
        try:
            shutil.rmtree(top_dir.as_posix())
        except OSError as e:
            if e.errno == 2:
                #The directory does not exist so pass.
                pass
            else:
                #Some other OSError occured.
                raise e

    top_dir.mkdir()

    #Now build the tree
    for child_dir in ["RPMS", "BUILD", "SPECS", "SRPMS"]:
        Path(top_dir, child_dir).mkdir()

def populate_spec_dir(path):
    """
    Fills the given path with dummy spec and txt files.
    """

    for x in range(1,5):
        filename = "file{}.spec".format(x)
        with open(Path(path, filename).as_posix(), "w+") as f:
            f.write(filename)

    for x in range(1,5):
        filename = "file{}.txt".format(x)
        with open(Path(path, filename).as_posix(), "w+") as f:
            f.write(filename)


class TestRPMBrewTopDirBase(unittest.TestCase):

    """A Base TestCase that creates an rpmbuild directory for use
    in other tests."""

    def setUp(self):

        if not hasattr(self, "top_dir"):
            self.top_dir = Path("/var/tmp/rpmbrew/")

        if not isinstance(self.top_dir, Path):
            self.top_dir = Path(self.top_dir)

        if not hasattr(self, "SPECS"):
            self.SPECS  = Path(self.top_dir, "SPECS")


        if not hasattr(self, "RPMS"):
            self.RPMS = Path(self.top_dir, "RPMS")

        #Ensure that our test directory does not exist.

        make_mock_rpmbuild_dir(self.top_dir)
        populate_spec_dir(self.SPECS)

    def tearDown(self):
        try:
            shutil.rmtree(self.top_dir.as_posix())
        except OSError:
            #The directory exists, we don't care.
            pass

    @property
    def default_spec_name(self):
        spec_filename = "{}.spec".format(self.top_dir.name)
        return spec_filename

class TestSpecFiles(TestRPMBrewTopDirBase):


    def test_list_specfiles(self):
        """
        tests the rpmbrew.specfils.listspecdir() to ensure it is able to
        get the list of .spec files in the given path.

        """

        os.chdir(self.SPECS.as_posix())

        with self.assertRaises(AttributeError):
            l = listspecdir()

        l = listspecdir(path=self.SPECS.as_posix())

        self.assertEqual(len(l),4)

class TestSpecFiles_find_specs_dir_in_children(TestRPMBrewTopDirBase):

    top_dir = "/var/tmp/rpmbrew_spectest_basic"


    def test_find_specs_dir_in_children(self):
        """
        A basic test, where we give it the top_dir. It should find the specs dir
        below the top_dir.
        """

        specs_dir = find_specs_dir_in_children(path=self.top_dir)
        self.assertEqual(specs_dir, Path(self.top_dir, "SPECS"))



class TestSpecFiles_find_specs_dir_in_children(TestRPMBrewTopDirBase):

    top_dir = "/var/tmp/rpmbrew_spectest"

    def test_find_specs_dir_in_children_1(self):
        """
        A basic test, where we give it the top_dir. It should find the spes dir
        below the top_dir. In this case, we test if there are more directories
        inside the top_dir.
        """
        try:
            Path(self.top_dir, "ADIR").mkdir()
        except OSError as e:
            #The dirs could have been created and not cleaned up.
            pass

        try:
            Path(self.top_dir, "BDIR").mkdir()
        except OSError as e:
            #The dirs could have been created and not cleaned up.
            pass

        try:
            Path(self.top_dir, "CDIR").mkdir()
        except OSError as e:
            #The dirs could have been created and not cleaned up.
            pass

        specs_dir = find_specs_dir_in_children(path=self.top_dir)
        self.assertEqual(specs_dir, Path(self.top_dir, "SPECS"))

        #Clean Up so as not to pollute the other tests.
        Path(self.top_dir, "ADIR").rmdir()
        Path(self.top_dir, "BDIR").rmdir()
        Path(self.top_dir, "CDIR").rmdir()


class TestSpecFiles_find_specs_dir_in_siblings(TestRPMBrewTopDirBase):

    top_dir = "/var/tmp/specs_dir_in_siblings"
    def setUp(self):
        super(TestSpecFiles_find_specs_dir_in_siblings, self).setUp()

        #Also create our RPMS directory

        try:
            Path(self.top_dir, "RPMS").mkdir()
        except OSError as e:
            pass

        try:
            Path(self.top_dir, "RPMS/x86_64").mkdir()
        except OSError as e:
            pass

        try:
            Path(self.top_dir, "RPMS/noarch").mkdir()
        except OSError as e:
            pass


    def test_find_specs_dir_in_siblings(self):

        path=Path(self.top_dir, "RPMS")

        specs_dir = find_specs_dir_in_siblings(path=path)
        self.assertEqual(specs_dir, self.SPECS)


    def tearDown(self):


        try:
            Path(self.top_dir, "RPMS").mkdir()
        except OSError as e:
            pass

        try:
            Path(self.top_dir, "RPMS/x86_64").mkdir()
        except OSError as e:
            pass

        try:
            Path(self.top_dir, "RPMS/noarch").mkdir()
        except OSError as e:
            pass


class Test_Success_find_spec_file(TestRPMBrewTopDirBase):

    top_dir = "/var/tmp/rpmbrew_spec"

    def setUp(self):

        super(Test_Success_find_spec_file, self).setUp()
        spec_filename = "{}.spec".format(self.top_dir.name)
        """We create a dummy spec file that has the same name as the top_dir"""
        with open(Path(self.SPECS, spec_filename).as_posix() , "w+") as f:
            f.write("dummy spec file")



    def test_find_specfile_when_in_SPEC_dir_with_no_name(self):


        result = find_spec_file(path=self.SPECS)
        spec_filename = "{}.spec".format(self.top_dir.name)
        self.assertEqual(result, Path(self.SPECS, spec_filename))


    def test_find_specfile_when_in_RPMS_dir_with_no_name(self):
        specs_dir = Path(self.top_dir, "SPECS")
        rpms_dir = Path(self.top_dir, "RPMS")

        result = find_spec_file(path=rpms_dir)
        self.assertEqual(result, Path(self.SPECS, self.default_spec_name))


class Test_Fail_find_spec_file(TestRPMBrewTopDirBase):

    top_dir = "/var/tmp/rpmbrew_test"


    def test_find_specfile_when_in_SPEC_dir_with_no_name(self):

        with self.assertRaises(SpecFileNotFound):

            result = find_spec_file(path=self.SPECS)
            spec_filename = "{}.spec".format(self.top_dir.name)
            self.assertEqual(result, Path(self.SPECS, spec_filename))

    def test_find_specfile_when_in_SPEC_dir_with_no_name(self):

        with self.assertRaises(SpecFileNotFound):

            path = Path(self.top_dir, "RPMS/x86_64/")
            path.mkdir()
            result = find_spec_file(path=path)
            spec_filename = "{}.spec".format(self.top_dir.name)
            self.assertEqual(result, Path(self.SPECS, spec_filename))



class Test_find_top_dir(TestRPMBrewTopDirBase):

    top_dir = "/var/tmp/rpmbrew_test"

    def test_find_top_dir_success(self):

        path = self.SPECS
        top_dir = find_top_dir(path)
        self.assertEqual(Path(self.top_dir), top_dir)




if __name__ == '__main__':
    unittest.main()