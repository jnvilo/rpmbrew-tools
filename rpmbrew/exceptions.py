
class FileDoesNotExist(Exception):
    pass

class NonStandardTarFileName(Exception):
    pass


class BrewGitUrlException(Exception):
    pass

class InvalidGitUrl(Exception):
    pass

class UnsupportedBuildOS(Exception):
    pass


class MultipleSpecFilesFound(Exception):
    def __init__(self, message, specfiles=None):

        # Call the base class constructor with the parameters it needs
        super(MultipleSpecFilesFound, self).__init__(message)



        # Now for your custom code...
        if not specfiles:
            self.specfiles = []
        else:
            if not isinstance(specfiles, list):
                raise TypeError("specfiles param must be of type: list.")
            self.specfiles = specfiles

class SpecFileNotFound(Exception):
    pass

class TopDirNotFound(Exception):
    pass