

def coerce_params(params_dict):
    """
    A simple decorator to convert the parameters to the given type in 
    the params_dict. 
    """
    
    def override_params_decorator(function):        

        def wrapper(*args, **kwargs):
            for key in params_dict.keys():
                if key in kwargs.keys():
                    obj_type = params_dict[key]
                    value = kwargs[key]
                    if not isinstance(value, obj_type):
                        casted_value = obj_type(value)
                        kwargs[key] = casted_value
                    
            return function(*args, **kwargs)
        return wrapper
    return override_params_decorator