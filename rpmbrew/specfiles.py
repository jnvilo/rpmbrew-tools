import pathlib
import os
from rpmbrew.decorators import coerce_params
from rpmbrew.exceptions import SpecFileNotFound
from rpmbrew.exceptions import TopDirNotFound

@coerce_params({ "path": pathlib.Path})
def listspecdir(path=None):
    """
    Gets a list of specfiles in the given path.
    """

    if path is None:
        raise AttributeError("path=None ! keyword argument path is required to have a value.")
    l = []
    for entry in path.iterdir():
        if entry.name.endswith(".spec"):
            l.append(entry)

    return l


def is_path_a_specs_dir(path):

    if path is None:
        raise ValueError("path param can not be None")

    if not isinstance(path, pathlib.Path):
        path = pathlib.Path(path)

    if path.name == "SPECS":
        return True
    else:
        return False

def find_specs_dir_in_children(path=None):
    """Finds the SPECS directory within the given starting location and searches
    recursively into the child directories"""

    if path is None:
        path = pathlib.Path(os.getcwd())


    #are we in the SPECS directory?
    if path.name == "SPECS":
        return path

    #Crap no we do not have. Enter the children

    for child_dir in path.iterdir():
        p = find_specs_dir_in_children(child_dir)
        if p:
            return p
    else:
        #None of the children have found a SPECS dir.
        return None


def find_spec_dir(path=None, height=3, depth=999):
    """"""
    if path is None:
        path = pathlib.Path(os.getcwd())


    #are we in the SPECS directory?
    if path.name == "SPECS":
        return path

    #Maybe we get lucky and the parent is the dir!
    if path.parent.name == "SPECS":
        return path.parent

    #Crap no we do not have. Enter the children

    specs_dir = find_specs_dir_in_children(path=path)

    if not specs_dir:
        specs_dir = find_specs_dir_in_siblings(path)

    if not specs_dir:
        # Crap the current path does not have the SPECS in its siblings
        # and neither does it have it in its parent.
        # So we ask our parent to check his siblings which will also
        # end up checking their children.
        c = 0
        while specs_dir is None:
            path= path.parent
            specs_dir = find_specs_dir_in_siblings()
            if path.name == "/":
                #Ok we have searched to the fucking top.
                raise SpecFileNotFound("Whole filesystem searched. This sucks.")
            if count == height:
                raise SpecFileNotFound("Maximum parents level reached.")

    #This function either returns succesfully or else dies trying.
    return specs_dir



def find_specs_dir_in_siblings(path=None):
    """
    Searches the siblings of the path to check if SPECS dir exists.
    """

    if is_path_a_specs_dir(path):
        return path

    """
    We iterate through the siblings of the path and go inside them to
    find the SPECS directory.
    """
    for child_dir in path.parent.iterdir():
        if child_dir != path:
            #We of course don't care about ourselves!
            if is_path_a_specs_dir(child_dir):
                return child_dir
            else:
                #If not, then maybe the dir has children. Lets look inside
                #the children.
                r = find_specs_dir_in_children(child_dir)
                if r is not None:
                    return r
    else:
        return None


@coerce_params({ "path": pathlib.Path})
def find_spec_file(path=None,specfile=None):
    """We need to know the spec file of what to download.

    This recursively goes up until it hits the .git or /
    file system looking for a SPECS directory.
    """





    if specfile is None:
        #Deal with case 1: We are inside a SPEC directory.
        if path.as_posix().endswith("SPECS"):
            l = listspecdir(path)

            # If we get more than 1 answer, then we'll choose the
            # the specfile name same as the top_dir.
            if len(l) > 1:
                assumed_specfile = path.parent.name + ".spec"
                if pathlib.Path(path, assumed_specfile) in l:
                    return pathlib.Path(path, assumed_specfile)
                else:
                    raise SpecFileNotFound("Could not automagically find a spec file to use.")
        else:
            #Do we have a child directory called SPECS?
            spec_path = pathlib.Path(path, "SPECS")
            if spec_path.exists():
                return find_spec_file(path=spec_path)

            else:
                #Keep going up the tree.
                if path.as_posix() != "/":
                    return find_spec_file(path.parent)
                else:
                    raise SpecFileNotFound("Hit / while trying magic to find a spec file to use.")



    if pathlib.Path(path, spec).is_file():
        #means the spec file is right here.
        return pathlib.Path(pat, spec)

    if not spec:
        #Do we have a SPECS dir wghere we are?
        cwd = os.getcwd()

        return find_spec(pathlib.Path(cwd, "SPECS"))


def find_top_dir(path):

    try:
        specs_dir = find_spec_dir(path=path)
    except SpecFileNotFound as e:
        raise TopDirNotFound("Could not automatically find top_dir. Please provide top_dir param and specfile name.")

    return specs_dir.parent