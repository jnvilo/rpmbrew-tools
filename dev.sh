#!/usr/bin/env python

virtualenv venv
source ./venv/bin/activate
pip install -e ../rpmbrew-tools/
